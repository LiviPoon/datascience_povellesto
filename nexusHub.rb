require 'csv'
require 'nokogiri'
require 'open-uri'
require 'webdrivers'
require 'watir'

begin

	browser = Watir::Browser.new
	table_array = []
	browser.goto("https://nexushub.co/warframe/items/frost-prime/prices"). #loads the page
	browser.element(xpath: "//div[contains(@class, 'warframe_module')]").wait_until(&:exists?) #waits until it’s fully loaded
	war = browser.element(xpath: "//div[contains(@class, 'warframe_module')]") #loads the first of the divs with what you are grabbing


	war.next_sibling.next_sibling do |row|

		row_array = Array.new
		row.cells.each do |cell|
		row_array << cell.text

		end
		puts "Row Array #{row_array}"
		table_array << row_array
	end
		puts table_array.to_yaml
	end

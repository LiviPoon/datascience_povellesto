require 'wombat'
require 'csv'

begin
	target_url = "https://nexushub.co/warframe/items/frost-prime/prices"
	# class	MavScraper
	bob = Wombat.crawl do
          base_url URI.escape(target_url)
          path "/"
            prods "xpath=//div[contains(@class, 'warframe_module')]", :iterator do
              nameOfItem xpath: ".//h3[@data-v-10455308='']"
							economyData xpath: ".//div[@class='economy']"
							previousPriceDate xpath: ".//text[@class='title']"
							previousPriceData xpath: ".//text[@class='num']"
							priceOfItem xpath: ".//span[contains(@class,'highlight')]"
            end
        end
        # puts bob["prods"]

		buyers = bob["prods"].each do |goodies|
		end

		# puts 	buyers.to_yaml

    CSV.open("prices.csv", "wb") do |csv|
		csv << ["nameOfItem", "priceOfItem", "buyers", "sellers"]
		bob["prods"].each do |prod|
			csv << [prod["nameOfItem"], prod["priceOfItem"], prod["economyData"].scan(/Buyers\W+(\d+)/).first.first, prod["economyData"].scan(/Sellers\W+(\d+)/).first.first]
		end
	end


end


# /\d+/
	#/Buyers\s*(\d+)/

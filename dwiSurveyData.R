library(ggplot2)

ggplot(data = dwiSurveyResults) + ggtitle("Sleep Vs. Other Things") +
  geom_smooth(mapping = aes(x = Sleepy, y = Hunger, color="Hunger")) +
  geom_smooth(mapping = aes(x = Sleepy, y = teacherSupport, color= "Teacher Support")) +
  geom_smooth(mapping = aes(x = Sleepy, y = friendSupport, color= "Friend Support")) +
  geom_smooth(mapping = aes(x = Sleepy, y = familySupport, color= "Family Support")) +
  geom_smooth(mapping = aes(x = Sleepy, y = feelingEmotions, color= "Feeling Emotions")) +
  geom_smooth(mapping = aes(x = Sleepy, y = physicalActivity, color= "Physical Activity"))
    


ggplot(data = dwiSurveyResults) + ggtitle("Hunger Vs. Other Things") +
  geom_smooth(mapping = aes(x = Hunger, y = feelingEmotions, color="Feeling Emotions")) +
  geom_smooth(mapping = aes(x = Hunger, y = teacherSupport, color= "Teacher Support")) +
  geom_smooth(mapping = aes(x = Hunger, y = friendSupport, color= "Friend Support")) +
  geom_smooth(mapping = aes(x = Hunger, y = familySupport, color= "Family Support"))

ggplot(data = dwiSurveyResults) + ggtitle("Friend Support Vs. Other Things") +
  geom_smooth(mapping = aes(x = friendSupport, y = feelingEmotions, color="Feeling Emotions")) +
  geom_smooth(mapping = aes(x = friendSupport, y = teacherSupport, color= "Teacher Support")) +
  geom_smooth(mapping = aes(x = friendSupport, y = Hunger, color= "Friend Support")) +
  geom_smooth(mapping = aes(x = friendSupport, y = familySupport, color= "Family Support"))



ggplot(data = dwiSurveyResults) + ggtitle("How Feeling Emotions Affect Me") +
  geom_smooth(mapping = aes(x = feelingEmotions, y = Hunger, color="Hunger")) +
  geom_smooth(mapping = aes(x = feelingEmotions, y = teacherSupport, color= "Teacher Support")) +
  geom_smooth(mapping = aes(x = feelingEmotions, y = friendSupport, color= "Friend Support")) +
  geom_smooth(mapping = aes(x = feelingEmotions, y = familySupport, color= "Family Support")) +
  geom_smooth(mapping = aes(x = feelingEmotions, y = physicalActivity, color= "Physical Activity"))+
  geom_smooth(mapping = aes(x = feelingEmotions, y = Sleepy, color= "Feeling Sleepy")) +
  geom_smooth(mapping = aes(x = feelingEmotions, y = expressingEmotions, color= "Expressing Emotion"))
 

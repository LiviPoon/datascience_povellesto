require 'wombat'
require 'csv'

begin
	target_url = "https://nexushub.co/warframe/items/frost-prime/prices"
	# class	MavScraper
	bob = Wombat.crawl do
          base_url URI.escape(target_url)
          path "/"
					prod "xpath=//div[@class='col inline-data interactive']", :iterator do
						overral xpath: "//label"
						overralData xpath: "//span[@class='data']"

						end

        end

        puts bob["prods"]

    CSV.open("prices.csv", "wb") do |csv|
		csv << ["nameOfItem", "priceOfItem", "economyData", "overral", "overralData"]
		bob["prods"].each do |prod|
			csv << [prod["overral"], prod["overralData"], target_url, Date.today]
		end
	end


end

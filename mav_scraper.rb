require 'wombat'
require 'csv'

begin
	target_url = "http://www.amazon.com/WOOLOVE-Extra-Thick-Blocking-mats/dp/B07KCL9KPJ/?th=1#customerReviews"
	# class	MavScraper
	bob = Wombat.crawl do 
          base_url URI.escape(target_url)
          path "/"
            prods "xpath=//div[contains(@id,'customer_review-')]", :iterator do 
              review_title xpath: ".//a[contains(@class,'review-title')]"
              review_date xpath: ".//span[contains(@class, 'review-date')]"
            end
        end

        puts bob["prods"]

    CSV.open("reviews.csv", "wb") do |csv|
		csv << ["title", "date", "review_page", "date_scraped"]
		bob["prods"].each do |prod|
			csv << [prod["review_title"], prod["review_date"], target_url, Date.today]
		end
	end


end
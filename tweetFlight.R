flightsV2 <- mutate(flights,
       gain = dep_delay - arr_delay,
       speed = distance / air_time * 60
)

ggplot(data = flightsV2) + 
  geom_point(mapping = aes(x = distance, y = speed, color = carrier, size = air_time))

ggplot(data = FLUDataNew) + 
  geom_point(mapping = aes(x = Week, y = TestedPositive, color = Country)) + 
  geom_smooth(mapping = aes(x = Week, y = TestedPositive, color = Country))
  

ggplot(data = FLUDataNew) + 
  geom_smooth(mapping = aes(x = Week, y = Received, color = Region))
  

FLUDataNew <- rename(FLUDataRevised,
       Week = Country.1)

FLUDataNew <- mutate(FLUDataNew, 
      DifferenceRP = Received - Processed,
      TestedNegative = Processed - TestedPositive)

ggplot(data = FLUDataNew) +
  geom_point(mapping = aes(x = TestedPositive, y = Processed, color = 'blue'))

ggplot(data = coronaVirus) +
  geom_point(mapping = aes(x = Total.deaths, y = Total.confirmed..cases))


